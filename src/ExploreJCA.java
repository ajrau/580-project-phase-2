import java.security.Provider;
import java.security.Security;

public class ExploreJCA {
    public static void main(String[] args) {
        
        //Looping through list of Provider Architecture
        for (Provider provider : Security.getProviders()) {
            System.out.println("Provider -> " + provider.getName());
            //Looping through Algorithms for each Provider
            for (Provider.Service service : provider.getServices()) {
                System.out.println("\t" + service.getAlgorithm());
            }
        }

    }
}