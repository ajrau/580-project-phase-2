/*
Alex Rau
CSC580
3/6/2017
*/
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class JCATime {

    public static void main(String[] args) throws 
            NoSuchAlgorithmException, NoSuchPaddingException, IOException {
        //Reused variables
        byte[] data;
        long start;
        long fin;
        long iter;
        float tp;

        //AES single-key encrypting 100000 bytes repeatedly
        data = new byte[100000];
        start = System.nanoTime();
        
        for (int i = 0; i < 100000; i++) {
            //Bouncer for the encryption being like "yeah bruh, you good"
            try {
                encAES(data);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadPaddingException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //Computation done to get throughput: Blocks / Second instead of Circles / Second
        fin = System.nanoTime() - start;
        iter = 100000;
        tp = (float) (iter * data.length) / fin;
        //Convert nanoseconds to seconds
        tp *= 1000000000;

        System.out.println("AES Single Key : ");
        System.out.println("  " + tp + " Blocks / Second");

        //AES random-key encrypting 16 bytes repeatedly
        data = new byte[16];
        start = System.nanoTime();

        for (int i = 0; i < 200000; i++) {
            //That friend who watches yo back from crazy peeps and illegal expressions
            try {
                encAESRanKeys(data);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadPaddingException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //Oh damn, more Blocks / Seconds.  Gonna overthrow Lego soon
        fin = System.nanoTime() - start;
        iter = 200000;
        tp = (float) (iter * data.length) / fin;
        //Convert nanoseconds to seconds
        tp *= 1000000000;

        System.out.println("AES Random Key : ");
        System.out.println("  " + tp + " Blocks / Second");

        //DES single-key encrypting 100000 bytes repeatedly
        data = new byte[100000];
        start = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            //Build a wall to keep the Illegal Expressions out!
            try {
                encAES(data);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadPaddingException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //Guess Who's back.  Back Again.  Blocks are back.  Tell a friend
        fin = System.nanoTime() - start;
        iter = 100000;
        tp = (float) (iter * data.length) / fin;
        //Convert nanoseconds to seconds
        tp *= 1000000000;

        System.out.println("DES Single Key : ");
        System.out.println("  " + tp + " Blocks / Second");

        //DES random-key encrypting 16 bytes repeatedly
        data = new byte[8];
        start = System.nanoTime();

        for (int i = 0; i < 200000; i++) {
            //You know those times where you're at a door and pull, but you actually gotta push?
            //Yeah these are like those, but with illegal expressions.
            try {
                encDESRanKeys(data);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadPaddingException ex) {
                Logger.getLogger(JCATime.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        fin = System.nanoTime() - start;
        iter = 200000;
        tp = (float) (iter * data.length) / fin;
        //Convert nanoseconds to seconds
        tp *= 1000000000;

        System.out.println("DES Random Key : ");
        System.out.println("  " + tp + " Blocks / Second");
    }
    //Encrypt Single Time AES
    public static byte[] encAES(byte data[]) throws 
            NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, 
            IllegalBlockSizeException, BadPaddingException {
        //Setting key size and specs
        byte[] keybytes = new byte[16];
        SecretKeySpec ks = new SecretKeySpec(keybytes, "AES");
        //Selecting AES and initializing
        Cipher aes = Cipher.getInstance("AES");
        aes.init(Cipher.ENCRYPT_MODE, ks);
        
        byte[] encData = aes.doFinal(data);
        return encData;
    }
    //Encrypt Random Key AES
    public static byte[] encAESRanKeys(byte data[]) throws NoSuchAlgorithmException, 
            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, 
            BadPaddingException {
        //Generating a key
        KeyGenerator kg = KeyGenerator.getInstance("AES");
        kg.init(128);
        SecretKey secKey = kg.generateKey();
        //Setting key size and specs
        byte[] keybytes = new byte[16];
        SecretKeySpec ks = new SecretKeySpec(keybytes, "AES");
        //Selecting AES and initializing
        Cipher aes = Cipher.getInstance("AES");
        aes.init(Cipher.ENCRYPT_MODE, secKey);

        byte[] encData = aes.doFinal(data);
        return encData;
    }
    //Encrypt Single Time DES
    public static byte[] encDES(byte data[]) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        //Setting key size and specs
        byte[] keybytes = new byte[7];
        SecretKeySpec ks = new SecretKeySpec(keybytes, "DES");
        //Selecting DES and initializing
        Cipher des = Cipher.getInstance("DES");
        des.init(Cipher.ENCRYPT_MODE, ks);

        byte[] encData = des.doFinal(data);
        return encData;
    }
    //Encrypt Random Key DES
    public static byte[] encDESRanKeys(byte data[]) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        //Generating a Key
        KeyGenerator kg = KeyGenerator.getInstance("DES");
        kg.init(56);
        SecretKey secKey = kg.generateKey();
        //Selecting key size and specs
        byte[] keybytes = new byte[7];
        SecretKeySpec ks = new SecretKeySpec(keybytes, "DES");
        //Selecting DES and initizalizing
        Cipher des = Cipher.getInstance("DES");
        des.init(Cipher.ENCRYPT_MODE, secKey);

        byte[] encData = des.doFinal(data);
        return encData;
    }

}
